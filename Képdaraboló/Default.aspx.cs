﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Képdaraboló
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void DarabolGomb_Click(object sender, EventArgs e)
        {
            Hiba.Text = "";
           Bitmap eredeti = new Bitmap(Server.MapPath("~") + "virág.jpg");
            try
            {
                int x = Convert.ToInt16(balfx.Text); int y = Convert.ToInt16(balfy.Text);
                int szélesség = Convert.ToInt16(szél.Text); int magasság = Convert.ToInt16(mag.Text);
                Képdarab.ImageUrl = eredeti.darabka(x, y, szélesség, magasság);
            }
            catch { Hiba.Text = "Túlmutat a kép területén vagy nem számot adott meg!"; }
        }
    }
}