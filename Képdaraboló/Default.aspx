﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Képdaraboló._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Képdaraboló</h1>          
    <div class="row">
        <div class="col-sx-12">
            <div class="row">
               <div class="col-md-9">
                 <asp:Image ID="EredetiKép" runat="server" ImageUrl="~/virág.jpg" />
                </div>   
                <div class="col-md-3 " style="margin-left:auto;margin-right:auto">
                  <asp:Image ID="Képdarab" runat="server" />
                 </div>   
             </div>   
              <div class="row">
                  <div class="col-md-3">
                   <Label for="balfx" >  X </Label>
                     
                   <asp:TextBox ID="balfx" runat="server" ClientIDMode="static" CssClass=" form-control">50</asp:TextBox>
                  </div>  
                  <div class="col-md-3">
                    <Label for="balfy" > Y </Label>
                    <asp:TextBox ID="balfy" ClientIDMode="static" runat="server" CssClass="form-control">50</asp:TextBox>
                   </div>  
                  <div class="col-md-3">
                    <Label for="szél" >  szélesség </Label>
                    <asp:TextBox ID="szél" ClientIDMode="static" runat="server" CssClass="form-control">200</asp:TextBox>
                    </div>  
                  <div class="col-md-3">
                       <Label for="mag" > magasság </Label>
                      <asp:TextBox ID="mag" ClientIDMode="static" runat="server" CssClass="form-control">200</asp:TextBox>
                   </div>
                 </div>
                <div class="row" >   
                     <div class="col-md-3" style="padding-top:10px">
                        <asp:Button ID="DarabolGomb" runat="server" Text="Daraboljon" CssClass="btn btn-normal" OnClick="DarabolGomb_Click" />    
                     </div>
                    <div class="col-md-9" style="padding-top:10px">
                        <asp:Label ID="Hiba" runat="server" Text="" CssClass="label label-warning" />    
                    </div>
              </div>    
        </div>
     
    </div>

</asp:Content>
