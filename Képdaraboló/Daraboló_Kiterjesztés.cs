﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;

namespace Képdaraboló
{
    static public class Daraboló_Kiterjesztés
    {
       static public string darabka(this Bitmap eredeti, int x , int y, int szélesség, int magasság)
        {
                      
            Bitmap képdarabka = new Bitmap(szélesség, magasság);
            for (int i = 0; i < szélesség; i++)
            {
                for (int j = 0; j < magasság; j++)
                {
                    képdarabka.SetPixel(i, j, eredeti.GetPixel(x + i, y + j));
                }
            }
            MemoryStream ms = new MemoryStream();
            képdarabka.Save(ms, ImageFormat.Gif);
            var base64Data = Convert.ToBase64String(ms.ToArray());
           
            return "data:image/gif;base64," + base64Data;

        } 

    }
}